#include "stdafx.h"
#include "Residential.h"


Residential::Residential(string createdAddressProperty, bool createdRental, double createdValue, bool createdOccupied, string createdAddress) : Estate()
{
	addressProperty = createdAddressProperty;
	rental = createdRental;
	value = createdValue;
	occupied = createdOccupied;
	address = createdAddress;
}


Residential::~Residential()
{
}

string const Residential::toString()
{
	stringstream ss;
	ss << "Address: " << address << " ";
	if (rental)
	{
		ss << "Rental ";
	}
	else
	{
		ss << "NOT rental ";
	}
	ss << "Estimated value: " << value << " ";
	if (occupied)
	{
		ss << "Occupied" << endl;
	}
	else
	{
		ss << "NOT occupied" << endl;
	}
	return ss.str();
}

double const Residential::calculateTaxes()
{
	if (occupied)
	{
		return occupiedResidentialTax * value;
	}
	else
	{
		return nonOccupiedResidentialTax * value;
	}
}