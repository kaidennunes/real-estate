#pragma once
#include "Estate.h"

class Commercial :
	public Estate
{
protected:
	double taxDiscount;
	bool ifDiscounted;
	const double rentalCommercialTax = .0012;
	const double nonRentalCommercialTax = .01;
public:
	Commercial(string createdAddressProperty, bool createdRental, double createdValue, bool createdifDiscounted, double createdTaxDiscount, string createdAddress);
	~Commercial();
	string const toString();
	double const calculateTaxes();
};

