#pragma once
#include "Estate.h"

class Residential :
	public Estate
{
protected:
	bool occupied;
	const double occupiedResidentialTax = .006;
	const double nonOccupiedResidentialTax = .009;
public:
	Residential(string createdAddressProperty, bool createdRental, double createdValue, bool createdOccupied, string createdAddress);
	~Residential();
	string const toString();
	double const calculateTaxes();
};

