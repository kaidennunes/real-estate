#include "stdafx.h"
#include "Commercial.h"


Commercial::Commercial(string createdAddressProperty, bool createdRental, double createdValue, bool createdifDiscounted, double createdTaxDiscount, string createdAddress) : Estate()
{
	addressProperty = createdAddressProperty;
	rental = createdRental;
	value = createdValue;
	ifDiscounted = createdifDiscounted;
	taxDiscount = createdTaxDiscount;
	address = createdAddress;
}


Commercial::~Commercial()
{
}

string const Commercial::toString()
{
	stringstream ss;
	ss << "Address: " << address << " ";
	if (rental)
	{
		ss << "Rental ";
	}
	else
	{
		ss << "NOT rental ";
	}
	ss << "Estimated value: " << value << " ";
	if (ifDiscounted)
	{
		ss << "Discounted ";
		ss << "Discount: " << taxDiscount << endl;
	}
	else
	{
		ss << "NOT discounted" << endl;
	}
	return ss.str();
}

double const Commercial::calculateTaxes()
{
	double taxAmount = value;
	// If discounted, apply that
	if (ifDiscounted)
	{
		taxAmount *= (1 - taxDiscount);
	}
	// Get the appropiate amount of taxes for rentals/non rentals
	if (rental)
	{
		taxAmount *= rentalCommercialTax;
	}
	else
	{
		taxAmount *= nonRentalCommercialTax;
	}
	return taxAmount;
}
