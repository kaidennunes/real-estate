#pragma once

#include "stdafx.h"
#include <iostream>
#include <sstream>

using namespace std;

class Estate
{
protected:
	string addressProperty;
	bool rental;
	double value;
	string address;

public:
	Estate();
	~Estate();
	double getValue() const;
	string getAddress() const;
	virtual const string toString() = 0;
	virtual const double calculateTaxes() = 0;
};

