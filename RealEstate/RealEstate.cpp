// RealEstate.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Estate.h"
#include "Commercial.h"
#include "Residential.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;
/*
			TEST CASE #1

			What file do you want to open?
			case1

			Ignoring bad Commercial in input file: Commercial 0 23343.2  We don't know

			All valid properties:
			Property id: 0 Address: 142 Kids Driving NOT rental Estimated value: 250000 Occupied
			Property id: 1 Address: 350 Somewhere Fun Rental Estimated value: 1.23457e+006 Occupied
			Property id: 2 Address: 842 Taxing Road Rental Estimated value: 4.5679e+006 Discounted Discount: 0.2
			Property id: 3 Address: 572 A Place Rental Estimated value: 533000 NOT occupied


			Now printing tax report:
			** Taxes due for the property at: 142 Kids Driving
			Property id: 0
			This property has an estimated value of: 250000
			Taxes due on this property are: 1500
			** Taxes due for the property at: 350 Somewhere Fun
			Property id: 1
			This property has an estimated value of: 1.23457e+006
			Taxes due on this property are: 7407.4
			** Taxes due for the property at: 842 Taxing Road
			Property id: 2
			This property has an estimated value of: 4.5679e+006
			Taxes due on this property are: 4385.18
			** Taxes due for the property at: 572 A Place
			Property id: 3
			This property has an estimated value of: 533000
			Taxes due on this property are: 4797

			Press any key to continue . . .



			TEST CASE #2
			What file do you want to open?
			case2

			Ignoring bad Commercial in input file: Commercial 23343.2 .2 Not right
			Ignoring unknown types of properties appearing in the input file: Bob's Lane
			Ignoring bad Commercial in input file: Commercial 0 .2 Something

			All valid properties:
			Property id: 0 Address: 950 Ocean Beach NOT rental Estimated value: 1.23457e+006 Discounted Discount: 0.1
			Property id: 1 Address: 572 Is this the right way Rental Estimated value: 533000 NOT occupied
			Property id: 2 Address: 102 I don't care NOT rental Estimated value: 250000 Occupied
			Property id: 3 Address: 798 Somewhere Dr. Rental Estimated value: 2.64568e+006 NOT discounted


			Now printing tax report:
			** Taxes due for the property at: 950 Ocean Beach
			Property id: 0
			This property has an estimated value of: 1.23457e+006
			Taxes due on this property are: 11111.1
			** Taxes due for the property at: 572 Is this the right way
			Property id: 1
			This property has an estimated value of: 533000
			Taxes due on this property are: 4797
			** Taxes due for the property at: 102 I don't care
			Property id: 2
			This property has an estimated value of: 250000
			Taxes due on this property are: 1500
			** Taxes due for the property at: 798 Somewhere Dr.
			Property id: 3
			This property has an estimated value of: 2.64568e+006
			Taxes due on this property are: 3174.81

			Press any key to continue . . .



			TEST CASE #3
			What file do you want to open?
			case3

			Ignoring bad Commercial in input file: Commercial 0 .2 Something Else
			Ignoring unknown types of properties appearing in the input file: Roadkill 06 .2

			All valid properties:
			Property id: 0 Address: 564 Out of names Rental Estimated value: 4.89758e+006 NOT occupied
			Property id: 1 Address: 456 Not named NOT rental Estimated value: 23343 NOT discounted
			Property id: 2 Address: 946 Another Place NOT rental Estimated value: 1.54898e+006 Occupied
			Property id: 3 Address: 313 Cherry Lane Rental Estimated value: 4.59723e+006 Discounted Discount: 0.2
			Property id: 4 Address: 742 Fast Driving NOT rental Estimated value: 8.97556e+006 Discounted Discount: 0.1


			Now printing tax report:
			** Taxes due for the property at: 564 Out of names
			Property id: 0
			This property has an estimated value of: 4.89758e+006
			Taxes due on this property are: 44078.2
			** Taxes due for the property at: 456 Not named
			Property id: 1
			This property has an estimated value of: 23343
			Taxes due on this property are: 233.43
			** Taxes due for the property at: 946 Another Place
			Property id: 2
			This property has an estimated value of: 1.54898e+006
			Taxes due on this property are: 9293.85
			** Taxes due for the property at: 313 Cherry Lane
			Property id: 3
			This property has an estimated value of: 4.59723e+006
			Taxes due on this property are: 4413.34
			** Taxes due for the property at: 742 Fast Driving
			Property id: 4
			This property has an estimated value of: 8.97556e+006
			Taxes due on this property are: 80780.1

			Press any key to continue . . .
			*/
bool toBool(const std::string & s) {
	return s.at(0) == '1';
}

bool readFile(vector<Estate*> &estates)
{
	// This is so we can quit the program if we enter a file that doesn't exist
	bool continueProgram = true;
	string fileName;
	cin >> fileName;

	string line;
	ifstream myfile(fileName + ".txt");
	cout << endl;
	if (myfile.is_open())
	{
		// These are the values we need to find
		string addressProperty;
		bool rental;
		double value;
		bool occupiedOrIfDiscounted;
		double taxDiscount;
		string address;

		while (getline(myfile, line))
		{
			if (line.length() == 0)
			{
				cout << "Ignoring empty input in input file:" << endl;
			}
			else
			{
				// This is so we can identify a bad line (if it isn't commercial or residential)
				bool unknownTypes = false;

				// This is so we can print out the invalid line later, if we find out it was invalid (since we modify the original line)
				string inputLine = line;

				// This is how we parse through the words
				string delimiter = " ";
				size_t pos = 0;
				string token;
				int position = 1;

				// Number of words we need to read before we reach the address (is the address if not commercial)
				int numberOfWordsToRead = 5;
				bool noErrors = true;
				while (position <= numberOfWordsToRead && noErrors)
				{
					// If for some reason we can't finish (the line is invalid) we can stop and print out an error
					try
					{
						pos = line.find(delimiter);
						token = line.substr(0, pos);
						if (position == 1)
						{
							// Find whether residential or commercial
							addressProperty = token;
							if (addressProperty.compare("Commercial") == 0)
							{
								// If commercial, we will need to find the income tax, so there will be another word to read
								numberOfWordsToRead++;
							}
							else if (!(addressProperty.compare("Residential") == 0))
							{
								// If it isn't residential or commercial, then we have an unknown type and we must quit this line we are parsing
								unknownTypes = true;
							}
						}
						else if (position == 2)
						{
							if (stod(token) == 0 || stod(token) == 1)
							{
								// Set whether it is a rental
								rental = toBool(token);
							}
							else
							{
								noErrors = false;
							}
						}
						else if (position == 3)
						{
							// Set the value of the property
							value = stod(token);
						}
						else if (position == 4)
						{
							if (stod(token) == 0 || stod(token) == 1)
							{
								// Set whether it is occupied/discounted
								occupiedOrIfDiscounted = toBool(token);
							}
							else
							{
								noErrors = false;
							}
						}
						else if (position == 5)
						{
							// If commercial, this is when we find the tax discount
							if (addressProperty.compare("Commercial") == 0)
							{
								taxDiscount = stod(token);
								line.erase(0, pos + 1);
							}
							// Now set the address which is everything left over
							token = line.substr(0, line.length());
							address = token;
						}
						line.erase(0, pos + 1);
						position++;
					}
					catch (exception Ex)
					{
						// If we run into problems, then we have an error, so we stop trying to get the data from this line
						noErrors = false;
					}
				}
				if (noErrors && !(unknownTypes))
				{
					// This is where we create the pointer object and stick it into the vector
					Estate * newEstate;
					if (addressProperty.compare("Commercial") == 0)
					{
						newEstate = new Commercial(addressProperty, rental, value, occupiedOrIfDiscounted, taxDiscount, address);
					}
					else
					{
						newEstate = new Residential(addressProperty, rental, value, occupiedOrIfDiscounted, address);
					}
					estates.push_back(newEstate);
				}
				// If we had errors, we will print them out
				else
				{
					if (unknownTypes)
					{
						cout << "Ignoring unknown types of properties appearing in the input file: " << inputLine << endl;
					}
					else
					{
						cout << "Ignoring bad " << addressProperty << " in input file: " << inputLine << endl;
					}
				}
			}
		}
		myfile.close();

	}
	else
	{
		cout << "\nThe file could not be opened" << endl << endl;
		continueProgram = false;
	}
	return continueProgram;
}

void printValidProperties(vector<Estate*> estates)
{
	cout << "\nAll valid properties:" << endl;
	for (int i = 0; i < estates.size(); i++)
	{
		cout << "Property id: " << i << " " << estates[i]->toString();
	}
	cout << endl;
}

void printTaxReports(vector<Estate*> estates)
{
	cout << "\nNow printing tax report:" << endl;
	for (int i = 0; i < estates.size(); i++)
	{
		cout << "\n** Taxes due for the property at: " << estates[i]->getAddress() << endl;
		cout << "\tProperty id: " << i << endl;
		cout << "\tThis property has an estimated value of: " << estates[i]->getValue() << endl;
		cout << "\tTaxes due on this property are: " << estates[i]->calculateTaxes() << endl;
	}
	cout << endl;
}

int main()
{
	vector<Estate*> estates;

	cout << "What file do you want to open?" << endl;

	// This opens the file and creates the pointers
	bool continueFile = readFile(estates);
	if (continueFile)
	{
		if (estates.size() > 0)
		{
			// Print out the properties
			printValidProperties(estates);

			// Then print out the tax report
			printTaxReports(estates);
		}
		else
		{
			cout << "There aren't any properties!" << endl;
		}
	}
	else
	{
		cout << "The program will now exit" << endl << endl;;
	}
	system("pause");
	return 0;
}